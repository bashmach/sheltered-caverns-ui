import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('x-card', 'Integration | Component | x card', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{x-card}}`);

  assert.equal(this.$('.card-title').text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{x-card title='card title' buttonText='button action'}}
  `);

  assert.equal(this.$('.card-title').text().trim(), 'card title');
});
