/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'peepchat',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },

    DS: {
      host: 'http://192.168.10.10:4000',
      namespace: 'api'
    }
  };

  if (environment === 'development') {
    // config/environment.js
    ENV.contentSecurityPolicy = {
      // Deny everything by default
      'default-src': "'none'",

      'script-src': ["'self'", "https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js"],

      // Allow fonts to be loaded from http://fonts.gstatic.com
      'font-src': ["'self'", "http://fonts.gstatic.com"],

      // Allow data (xhr/websocket) from api.mixpanel.com and custom-api.local
      'connect-src': ["'self'", "http://192.168.10.10:4000"],
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    ENV.DS.host = 'https://sheltered-caverns-api.herokuapp.com';
  }

  return ENV;
};
