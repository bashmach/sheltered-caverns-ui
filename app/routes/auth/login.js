import Ember from 'ember';

const { RSVP, Route } = Ember;

export default Route.extend({
    actions: {
        doLogin () {
            console.log('login');
        }
    },

    model() {
        return RSVP.hash({
            user: {
                email: '',
                password: ''
            }
        });
    }
});
