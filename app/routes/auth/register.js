import Ember from 'ember';

const {RSVP, Route} = Ember;

export default Route.extend({
    actions: {
        doRegister() {
            this.get('currentModel').user.save()
                .then(() => {
                    this.transitionTo('auth.login');
                });
        }
    },

    model() {
        return RSVP.hash({
            user: this.store.createRecord('user')
        });
    }
});
