import DS from 'ember-data';
import config from '../config/environment';

const { JSONAPIAdapter } = DS;

export default JSONAPIAdapter.extend({
    host: config.DS.host,
    namespace: config.DS.namespace,

    urlForCreateRecord(modelName/*, snapshot*/) {
        switch(modelName) {
            case 'user':
            case 'users':
                return this._super.apply(this, arguments).replace('users', 'register');
            default:
                return this._super(...arguments);
        }
    }
});
