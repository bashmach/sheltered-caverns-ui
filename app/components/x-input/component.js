import Ember from 'ember';

const { computed, Component } = Ember;

export default Component.extend({
    classNames: ['input-field'],
    type: 'text',
    showValidations: false,

    hasErrors: computed.notEmpty('errors.[]').readOnly(),
    _errorMessages: computed('showValidations', 'errors.[]', function () {
        if (!this.get('showValidations')) {
            return [];
        }

        return (this.get('errors') || []).join(', ');
    }),

    focusOut() {
        this._super(...arguments);
        this.set('showValidations', true);
    }
});
