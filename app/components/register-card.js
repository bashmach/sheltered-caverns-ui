import Ember from 'ember';
import { email, password, passwordConfirmation } from 'peepchat/utils/user-validations';
import { buildValidations } from 'ember-cp-validations';

const { Component } = Ember;

const Validations = buildValidations({
    'user.email': email,
    'user.password': password,
    'user.passwordConfirmation': passwordConfirmation
});

export default Component.extend(Validations, {
});
